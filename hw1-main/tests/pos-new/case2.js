function case2(x, y) 
{
    assume(x <= 0 && y <= 0);
    while(x <= 0)
    {
        invariant (x <= 0 || y >= 0)
        x = x + y;
        y = y + 1;
    }
    assert(y >= 0);
}