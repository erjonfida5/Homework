function case1(n) 
{
    assume(n >= 0);
    x = 0 ;
    s = 0 ;

    while (x <= n)
    {
        invariant (((s * 2) == x * (x - 1)) && (x <= n + 1));
        s = s + x ;
        x = x + 1 ;
    }

    assert((s * 2) == n * (n + 1));
}
