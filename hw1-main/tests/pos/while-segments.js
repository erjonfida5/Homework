function foo() 
{
    var x = 0;
    var y = 50;

    while (x <= 99) 
    {
        invariant(x + y >= 50 && x + y <= 200 && x >= 0 && y >= 0 && x <= 100 && y <= 100 && y >= 50 && x <= y);

        x = x + 1;
        if (x >= 51) 
        {
            y = y + 1;
        }

    }
    assert (y == 100);
}
