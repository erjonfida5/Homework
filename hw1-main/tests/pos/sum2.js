function sum(n) 
{
    var i = 1; 
    var sum = 0;
    assume(true);

    while (n >= i) 
    {
        
        var j = 1;
        while (i >= j)
        {
            invariant (((sum * 2) == j * (j - 1)) && (j <= i + 1));
            sum = sum + j;  
            j = j + 1;
            i = i + 1;
        }
        assert(sum >= 0);
    }
}
